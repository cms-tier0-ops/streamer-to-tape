# -*- coding: utf-8 -*-
"""
File model. Represents a file to be transferred.
Author: Juan Pablo Vinchira Salazar
Contact: cms-tier0-operations@cern.ch
"""

class File:

    def __init__(self):
        """
        Initialize an empty file

        Attributes:
            file_id (Integer): File id.
            path (String): File path.
            adler32 (String): String with the adler32 sum of the file.
            filesize (Integer): Filesize in bytes.
            archived (Boolean): True if the file is archived.
            staged (Boolean): True if the file is on disk.
            run_id (Integer): 6 digits integer that represents the parent T0 run.
        """

        self.file_id = None
        self.path = None
        self.adler32 = None
        self.filesize = None
        self.archived = False
        self.staged = False
        self.run_id = None
