# -*- coding: utf-8 -*-
"""
Job model. Represents a transfer job
Author: Juan Pablo Vinchira Salazar
Contact: cms-tier0-operations@cern.ch
"""

class Job:

    JOB_TYPE = {
        'archive':1,
        'retrieve':2,
        'purge':3
    }

    JOB_STATE = {
        'SUBMITTED':1,
        'ACTIVE':2,
        'FINISHED':3,
        'FINISHEDDIRTY':4,
        'FAILED':5,
        'CANCELED':6
    }

    def __init__(self):
        """
        Initialize an empty job

        Attributes:
            job_id (String UUID): Transfer job UUID.
            job_type (String): Job type. The available types are defined in the JOB_TYPE static dictionary.
            job_state (String): Job state. The available states are defined in the JOB_STATE static dictionary.
            start_date (Date): Date when the job was submitted.
            end_date (Date): Date when the state of the job was updated to a final state.
            run_id (Int): 6 digits integer that represents the parent T0 run.
            submit_file (String): Path to the job configuration file used to create the transfer job.
            active (Boolean): True if the job is active.
        """

        self.job_id = None
        self.job_type = None
        self.job_state = 'SUBMITTED'
        self.start_date = None
        self.end_date = None
        self.run_id = None
        self.submit_file = None
        self.active = True
