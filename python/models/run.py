# -*- coding: utf-8 -*-
"""
Run model. Represents a T0 run
Author: Juan Pablo Vinchira Salazar
Contact: cms-tier0-operations@cern.ch
"""

class Run:

    def __init__(self):
        """
        Initialize an empty run

        Attrs:
            run_id (Integer): 6 digits integer that represents a T0 run.
            archived (Boolean): True if the run is already archived.
            staged (Boolean): True if the run is on disk.
        """

        self.run_id = None
        self.archived = False
        self.staged = True
