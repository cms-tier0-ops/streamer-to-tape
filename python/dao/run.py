# -*- coding: utf-8 -*-
"""
Run DAO. Represents a T0 run data access object.
Author: Juan Pablo Vinchira Salazar
Contact: cms-tier0-operations@cern.ch
"""

# Utils
from utils.database import Database

# Models
from models.run import Run

class RunDAO:

    def __init__(self):
        """
        Creates a database connection for the new RunDao instance
        """
        self.db = Database()
        self.db.connect()

    def __del__(self):
        """
        Closes the database connection when the object is deleted
        """
        self.db.conn.close()

    def insert_run(self, run):
        """
        Inserts a new run on the database.

        Arguments:
            run (Run): Run object.
        """
        
        # Insert sentence to insert a new run
        sql = '''
            INSERT INTO run (run_id, archived, staged)
            VALUES (?, ?, ?);
        '''

        # Insert the new run on the database
        self.db.conn.execute(sql, (run.run_id, run.archived, run.staged))
        self.db.conn.commit()

    def get_run(self, run_id):
        """
        Get a run from the database.

        Arguments:
            run_id (Integer): 6 digits integer representing a T0 run.

        Return:
            run (Run): A Run instance with the specified run retrieved from the database. If the run doesn't exist, then None is returned.
        """

        # Sentence to retrieve a run from the database
        sql = '''
            SELECT archived, staged
            FROM run WHERE run_id = ?;
        '''

        # Retrieve the run from the database (only one)
        cursor = self.db.conn.execute(sql, (run_id,))
        row = cursor.fetchone()
        
        # If the run exists
        if row:
            # Creates a new Run instance with the information retrieved from the database
            run = Run()
            run.run_id = run_id
            run.archived = False if row[0] == 0 else True
            run.staged = False if row[1] == 0 else True

            # Return the new Run instance
            return run
        else:
            return None

    def set_archived(self, run_id):
        """
        Set a run as archived.

        Arguments:
            run_id (Integer): 6 digits integer representing a T0 run.
        """

        # Sentence to set a run to archived
        sql = '''
            UPDATE run SET archived = 1 WHERE run_id = ?;
        '''

        # Set the specified run to archived
        self.db.conn.execute(sql, (run_id,))
        self.db.conn.commit()

    def set_staged(self, run_id):
        """
        Set a run as staged.

        Arguments:
            run_id (Integer): 6 digits integer representing a T0 run.
        """

        # Sentence to set a run to staged
        sql = '''
            UPDATE run SET staged = 1 WHERE run_id = ?;
        '''

        # Set the specified run to staged
        self.db.conn.execute(sql, (run_id,))
        self.db.conn.commit()
