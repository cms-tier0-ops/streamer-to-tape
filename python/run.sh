#!/bin/bash

PYTHON_VENV=/afs/cern.ch/work/c/cmst0/private/scripts/streamer-to-tape-venv
WORKDIR=$PYTHON_VENV/streamer-to-tape/python
LOG=$WORKDIR/out.log
LOCKFILE=$WORKDIR/lock

cd $PYTHON_VENV
source bin/activate

cd $WORKDIR

if [[ ! -f $LOCKFILE ]]
then
    touch $LOCKFILE
    echo "$(date +'%Y-%m-%d %H:%M:%S') INFO: RUNNING" &>> $LOG
    python run.py &>> $LOG
    echo "$(date +'%Y-%m-%d %H:%M:%S') INFO: DONE" &>> $LOG
    rm $LOCKFILE
fi
