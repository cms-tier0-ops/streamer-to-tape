# -*- coding: utf-8 -*-
"""
Class to access the values stored in the configuration file
Author: Juan Pablo Vinchira Salazar
Contact: cms-tier0-operations@cern.ch
"""

from configparser import RawConfigParser as configparser

class Config:

    def __init__(self):
        """
        Create an object with all the values stored in the configuration files
        """

        # Creates a config parser instance
        config = configparser()

        # Loads the configuration file
        with open('config') as ifile:
            config.read_file(ifile)

        # Source server variables
        self.source = {
            'name':config.get('Source','name'),
            'server':config.get('Source','server'),
            'path':config.get('Source','path')
        }

        # Destination server variables
        self.destination = {
            'name':config.get('Destination','name'),
            'server':config.get('Destination','server'),
            'path':config.get('Destination','path')
        }

        # Application important paths
        self.paths = {
            'data':config.get('Paths','data'),
            'workdir':config.get('Paths','workdir'),
            'json':config.get('Paths','json'),
            'logs':config.get('Paths','logs')
        }

        # Application important files
        self.files = {
            'runs':config.get('Files','runs'),
            'database':config.get('Files','database'),
            'voms_proxy':config.get('Files','voms_proxy')
        }

        # Additional scripts
        self.scripts = {
            'fts3':config.get('Scripts','fts3'),
            'stager':config.get('Scripts','stager')
        }
