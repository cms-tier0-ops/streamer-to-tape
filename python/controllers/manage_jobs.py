# -*- coding: utf-8 -*-
"""
Jobs manager.
Author: Juan Pablo Vinchira Salazar
Contact: cms-tier0-operations@cern.ch
"""

from datetime import datetime
import subprocess

# Utils
from utils.config import Config

# Models
from models.job import Job

# DAO
from dao.job import JobDAO
from dao.file import FileDAO

class ManageJobs:

    def __init__(self):
        """
        Initialize the class global variables using the configuration file.

        Attributes:
            fts3 (String): Path of the FTS3 script wrapper.
            data_path (String): Relative path to read/write streamer files (it's the same on source and destination).
            source_server (String): Server where the source files are stored.
            source_path (String): Path where the streamer files are hosted.
        """
        config = Config()
        self.fts3 = config.scripts['fts3']
        self.data_path = config.paths['data']
        self.source_server = config.source['server']
        self.source_server = config.source['server'].split('?', 1)[0]
        self.source_path = config.source['path']

    def update_states(self):
        """
        Updates the state of the ongoing jobs.
        """

        # Get the running jobs
        job_dao = JobDAO()
        jobs = job_dao.get_running_jobs()

        file_dao = FileDAO()

        print('Retrieving jobs to update...')

        # Create a list to store the updated jobs
        new_states = []

        # For each running job
        for each_job in jobs:

            # Retrieve the status of the current job
            cmd = subprocess.Popen([self.fts3, 'get_job_status', each_job.job_id],
                                    stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            state, stderr = cmd.communicate()
            state = state.strip()

            # If there're no errors and the it's a valid job state
            if len(stderr) == 0 and state in Job.JOB_STATE:

                # If the state of the job has changed since the last execution
                if each_job.job_state != state:

                    # If the job is not running
                    if state != 'SUBMITTED' and state != 'ACTIVE':

                        # Updates the job ends date
                        each_job.end_date = datetime.now()

                        print('Updating job {0} successful files...'.format(each_job.job_id))

                        # Retrieve the list of files that failed to be transferred
                        cmd = subprocess.Popen([self.fts3, 'get_failed_files ', self.source_server, self.source_path,  self.data_path, each_job.job_id],
                                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                        failed_list, stderr = cmd.communicate()

                        # If the command was executed successfully
                        if len(stderr) == 0:

                            # Create a python list with the failed files
                            failed_list = failed_list.split('\n')

                            # If it's an archiving job
                            if each_job.job_type == 'archive':
                                # Sets the successful files as archived
                                file_dao.set_archived_inv(each_job.run_id, failed_list)
                            # If it's a retrieving job
                            elif each_job.job_type == 'retrieve':
                                # Sets the successful files as retrieved (Staged)
                                file_dao.set_staged_inv(each_job.run_id, failed_list)
                            else:
                                print('Invalid type {0} for job {1}'.format(each_job.job_type, each_job.job_id))

                        else:
                            print('Error updating successful files. {0}'.format(stderr))

                    # Adds the updated job to the new_states list
                    print('Job {0}: {1} -> {2}'.format(each_job.job_id, each_job.job_state, state))
                    new_states.append((Job.JOB_STATE[state],each_job.end_date,each_job.job_id))

            else:
                print('Error updating the state of the job {0}. {1}'.format(each_job.job_id, stderr))
                print('Received state: {0}'.format(state))

        # Updates the job states on the database
        print('Updating job states')
        job_dao.update_states(new_states)
        print('Done')
