#!/bin/bash

########################################
# FTS3 wrapper script
# This script abstracts some funciontalities and interaction with FTS3
# Author: Juan Pablo Vinchira Salazar
# Contact: cms-tier0-operations@cern.ch
########################################

# Get the list of files related to an specific run
function get_run_files {

	# Base directory of streamers, for example "/eos/cms/store/t0streamer"
	base_dir=$2

	# Run id (6 digits)
	run_id=$3

	# List files for each folder containing the specified run number
	for each_folder in $(ls -d $base_dir/*/000/$(echo $run_id | sed -e 's/^\([0-9]\{3\}\)/\1\//')); do ls $each_folder/*; done
}

# Get a comma separated values list of a run's files (Filesize, Path, Adler32 sum)
function get_run_files_csv {

	# Base directory of streamers, for example "/eos/cms/store/t0streamer"
	base_dir=$2

	# Run id (6 digits)
	run_id=$3

	# List files for each folder containing the specified run number (Filesize, Path)
	for i in $(echo "$(for each_folder in $(ls -d $base_dir/*/000/$(echo $run_id | sed -e 's/^\([0-9]\{3\}\)/\1\//')); do ls -l $each_folder/*; done)" | sed -e 's/  */,/g' | cut -d ',' -f 9,5)
	do
		# Get the files contained source file path
		sfile=$(echo $i | cut -d , -f 2 | sed -e 's;^.*\(/store/.*$\);\1;')

		# Retrieve the Adler32 sum of the current file
		adler32=$(xrdfs eoscms.cern.ch query checksum $sfile | sed -e 's/^.* \([^ ]*\)$/\1/')

		# Append the Adler32 sum to the current file (Filesize, Path, Adler32 sum)
		echo $i,$adler32
	done
}

# Get the adler32 sum of an specific file
function get_adler32 {

		# File to query
		sfile=$2

		# Retrieve the Adler32 sum of the specified file
		xrdfs eoscms.cern.ch query checksum $sfile | sed -e 's/^.* \([^ ]*\)$/\1/'
}

# Submit a job on FTS3
function submit_job {

	# FTS3 submit JSON file
	input_file=$2

	# Submit the JSON job to FTS3 and store the new JOB id
	job_id=$(fts-transfer-submit \
	-s https://fts3.cern.ch:8446 \
	--json-submission \
	-o \
	--retry-delay 60 \
	--retry 4 \
	-f $input_file)

	# SED pattern to check if the string is a UUID
	sed_uuid_pattern='[0-9a-z]\{8\}-[0-9a-z]\{4\}-[0-9a-z]\{4\}-[0-9a-z]\{4\}-[0-9a-z]\{12\}'

	# IF pattern to check if the string is a UUID
	if_uuid_pattern=$(echo $sed_uuid_pattern | sed -e 's/\\//g')

	# Check if the Job id is an UUID
	if [[ $job_id =~ .*$if_uuid_pattern.* ]]
	then
		# Print the UUID of the new job
		echo "$job_id" | sed -e "s/.*\($sed_uuid_pattern\).*/\1/"
	else
		# Print an error message
		echo "$job_id" >&2
	fi
}

# Get a job status
function get_job_status {

	# UUID of a job
	job_id=$2

	# Use FTS3 to check the status of the job
	status=$(fts-transfer-status -s https://fts3.cern.ch:8446 $job_id)

	# Print the status of the job
	echo $status | sed -e 's/^\([^ ]*\).*/\1/'
}

# Get a list of the failed files of a finished job
function get_failed_files {

	# Source server, for example gsiftp://eoscmsftp.cern.ch
	server=${2%/}

	# Source streamer files path prefix, for example /eos/cms/store/t0streamer
	prefix=${3%/}

	# Streamer files path (relative path), for example Data
	data_path=${4#/}

	# Job UUID
	job_id=$5

	# Retrieve the failed files list of a transfer job (FTS3 creates a file in the folder where the command is executed)
	fts-transfer-status -s https://fts3.cern.ch:8446 -F $job_id &> /dev/null

	# Clean the line to keep only the filename with the data path
	cat $job_id | sed -e "s;^.*$server$prefix\(/$data_path[^ ]*\).*$;\1;"

	# Remove the FTS3 file list files
	rm $job_id
}

# Name of the function to execute
func=$1

# Executes the function with the parameters received by the script
$func "$@"
