#!/bin/bash

########################################
# STAGER wrapper script
# This script abstracts some funciontalities and interaction with CASTOR Stager
# Author: Juan Pablo Vinchira Salazar
# Contact: cms-tier0-operations@cern.ch
########################################

# Get the list of files related to an specific run
function get_state {

	# File path
	file=$2

	# Checks the staging state of a file
	output=$(stager_qry -M "$2" -S t0cms)

	# Check if it's an error message
	echo $output | grep -q -e '^Error.*$'

	# If there aren't error messages
	if [[ $? -ne 0 ]]
	then
		# Print current staging state
		echo "$output" | sed -e 's/  */ /g' | cut -d ' ' -f 3
	else
		# Notify that there's an error
		echo 'ERROR'
		>&2 echo "$output"
	fi
}

# Attempts to prestage a file
function prestage {

	# File path
	file=$2

	# Executes the prestage command
	output=$(stager_get -M "$2" -S t0cms)

	# Checks if the request is ok
	echo $output | grep -q -e '^.* SUBREQUEST_READY$'

	# Checks if the request was made successfully
	if [[ $? -eq 0 ]]
	then
		# Notify
		echo 'SUBREQUEST_READY'
	else
		# Notify about the error
		echo 'ERROR'
		>&2 echo "$output"
	fi
}

# Name of the function to execute
func=$1

# Executes the function with the parameters received by the script
$func "$@"
